﻿using System;

namespace Mego.Search.Model.Models
{
    public class ServiceMetrics
    {
        public string SystemName { get; set; }
        public TimeSpan ElapsedTime { get; set; }
    }
}
