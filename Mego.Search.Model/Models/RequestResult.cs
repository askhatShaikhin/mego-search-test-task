﻿namespace Mego.Search.Model.Models
{
    public class RequestResult
    {
        public string SystemName { get; set; }
        public string Result { get; set; }
    }
}
