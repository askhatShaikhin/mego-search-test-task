﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Mego.Search.Api.Extensions.Swagger
{
    public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>
    {
        public void Configure(SwaggerGenOptions options)
        {
            options.SwaggerDoc("v1", new OpenApiInfo()
            {
                Title = "Mego Search - HTTP API",
                Version = "v1",
                Description = "The Catalog of Microservices - Mego Search",
                Contact = new OpenApiContact() { Name = "To Me", Email = "shaikhinab@gmail.com" }
            });
        }
    }
}
