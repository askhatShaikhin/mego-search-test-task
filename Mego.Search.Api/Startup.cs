using System.Collections.Generic;
using Mego.Search.Api.Extensions.Swagger;
using Mego.Search.DataAccess;
using Mego.Search.Impl;
using Mego.Search.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Mego.Search.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson();

            services.ConfigureSwagger();

            services.AddSingleton<IMetricsService, MetricsService>();

            services.AddTransient<ISearchEngine, SearchEngine>();            
            services.AddTransient<IExternalA, ExternalA>();
            services.AddTransient<IExternalB, ExternalB>();
            services.AddTransient<IExternalC, ExternalC>();
            services.AddTransient<IExternalD, ExternalD>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swagger, httpReq) =>
                {
                    swagger.Servers = new List<OpenApiServer>
                    {
                        new OpenApiServer
                        {
                            Url = "https://localhost:44301"
                        }
                    };
                });
            });
            app.UseSwaggerUI(c=>
            {
                c.SwaggerEndpoint("../swagger/v1/swagger.json", "Mego API");
            });

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
