﻿using System.Net.Mime;
using Mego.Search.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Mego.Search.Api.Controllers
{
    /// <summary>
    /// Metrics.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MetricsController : ControllerBase
    {
        private readonly IMetricsService _metricsService;

        public MetricsController(IMetricsService metricsService)
        {
            _metricsService = metricsService;
        }

        /// <summary>
        /// Метрики
        /// </summary>
        /// <response code="200">Вернет результат</response>
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(object), 200)]
        [ProducesResponseType(204)]
        [HttpGet("metrics")]
        public IActionResult GetMetrics()
        {
            return Ok(_metricsService.GetMetrics());
        }
    }
}