﻿using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;
using Mego.Search.Interfaces;
using Mego.Search.Model.Models;
using Microsoft.AspNetCore.Mvc;

namespace Mego.Search.Api.Controllers
{
    /// <summary>
    /// Search.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class SearchController : ControllerBase
    {

        private readonly ISearchEngine _searchEngine;

        public SearchController(ISearchEngine searchEngine)
        {
            _searchEngine = searchEngine;
        }

        /// <summary>
        /// Поиск
        /// </summary>
        /// <response code="200">Вернет результат</response>
        /// <response code="400">Ошибка входных данных</response>
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(typeof(List<RequestResult>), 200)]
        [HttpGet("result")]
        public async Task<IActionResult> GetSearchResult()
        {
            var result = await _searchEngine.Search();
            return Ok(result);
        }
    }
}