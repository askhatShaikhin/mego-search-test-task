﻿using System;

namespace Mego.Search.Interfaces
{
    public interface IMetricsService
    {
        object GetMetrics();
        void SaveMetrics(string systemName, TimeSpan elapsedTime);
    }
}
