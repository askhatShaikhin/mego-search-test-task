﻿using Mego.Search.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mego.Search.Interfaces
{
    public interface ISearchEngine
    {
        Task<List<RequestResult>> Search();
    }
}
