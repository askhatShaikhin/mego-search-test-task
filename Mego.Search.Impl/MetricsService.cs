﻿using Mego.Search.Interfaces;
using Mego.Search.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mego.Search.Impl
{
    public class MetricsService : IMetricsService
    {
        private List<ServiceMetrics> _metrics;
        private List<ServiceMetrics> Metrics
        {
            get { return _metrics ?? (_metrics = new List<ServiceMetrics>()); }
            set => _metrics = value;
        }

        public object GetMetrics()
        {
            return Metrics.Select(x => new { x.SystemName, x.ElapsedTime.Seconds }).GroupBy(g => new { g.Seconds, g.SystemName }).OrderBy(o => o.Key.SystemName).ToList();
        }

        public void SaveMetrics(string systemName, TimeSpan elapsedTime)
        {
            var serviceMetrics = new ServiceMetrics
            {
                SystemName = systemName,
                ElapsedTime = elapsedTime
            };

            Metrics.Add(serviceMetrics);
        }
    }
}
