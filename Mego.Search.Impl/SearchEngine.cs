﻿using Mego.Search.DataAccess;
using Mego.Search.Interfaces;
using Mego.Search.Model.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Mego.Search.Impl
{
    public class SearchEngine : ISearchEngine
    {
        private readonly IExternalA _externalA;
        private readonly IExternalB _externalB;
        private readonly IExternalC _externalC;
        private readonly IExternalD _externalD;
        private readonly IMetricsService _metricsService;

        public SearchEngine(IExternalA externalA, IExternalB externalB, IExternalC externalC, IExternalD externalD, IMetricsService metricsService)
        {
            _externalA = externalA;
            _externalB = externalB;
            _externalC = externalC;
            _externalD = externalD;
            _metricsService = metricsService;
        }

        public async Task<List<RequestResult>> Search()
        {
            var result = new List<RequestResult>();

            var tasks = new List<Task>()
            {
                new Task(() => 
                {
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();

                    result.Add(_externalA.Request());

                    stopwatch.Stop();
                    _metricsService.SaveMetrics("A", stopwatch.Elapsed);
                }),
                new Task(() =>
                {
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();

                    result.Add(_externalB.Request());

                    stopwatch.Stop();
                    _metricsService.SaveMetrics("B", stopwatch.Elapsed);
                }),
                new Task(() =>
                {
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();

                    var cSysResult = _externalC.Request();

                    stopwatch.Stop();
                    _metricsService.SaveMetrics("C", stopwatch.Elapsed);

                    result.Add(cSysResult);
                    if (cSysResult.Result.Equals("OK")) result.Add(_externalD.Request());
                })
            };
            Parallel.ForEach(tasks, task => task.Start());

            Task.WaitAll(tasks.ToArray(), 4900);

            return result;
        }
    }
}
