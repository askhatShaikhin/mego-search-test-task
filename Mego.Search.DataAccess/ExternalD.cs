﻿using Mego.Search.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Troschuetz.Random.Generators;

namespace Mego.Search.DataAccess
{
    public class ExternalD : IExternalD
    {
        public RequestResult Request()
        {
            var random = new NR3Q1Generator();
            var taskExecTime = random.Next(1, 10) * 1000;
            var requestResult = random.Next(0, 2);

            Thread.Sleep(taskExecTime);

            var result = new RequestResult
            {
                SystemName = "D",
                Result = requestResult == 1 ? "OK" : "ERROR"
            };

            return result;
        }
    }
}
